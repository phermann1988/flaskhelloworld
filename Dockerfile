FROM python
WORKDIR /app
COPY app /app
RUN pip install -r requirements.txt
ENTRYPOINT [ "python" ]
CMD [ "hello_world.py" ]
