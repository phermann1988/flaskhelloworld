from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return '<h1>HALLO FLASK!\n</h1>'

@app.route('/about')
def about():
    return '<h1>ABOUT ME...!\n</h1>'

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
